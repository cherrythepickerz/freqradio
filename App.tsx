import React from 'react';
import {SafeAreaView, View} from 'react-native';
import Radio from './src/features/Radio';
import 'react-native-gesture-handler';

export default () => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: 'black',
      }}>
      <Radio />
    </SafeAreaView>
  );
};
