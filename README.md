# Freq Radio

A react-native based UI compoents for Radio screen feature.
Including:

- Screen layout
- Radio card (title, description, image-background
- Animated schedule component
- 'Time passed' component-timer

# UI stack

- Layout - common RN's components: View, StyleSheet, SafeAreaView
- State management - RN's custom hook
- Animation - react-native-reanimated v2
- Testing - @testing-library/react-hooks, @testing-library/react-native
- Utils: moment-js (for time operations),

# Feature descripton

The Radio screen contains a list of Radios that user can listen. Each Radio can have 2 states - `live` or `not live`.
If Radio is live, bakground color of bottom part is turning into 'purple', based on the schedule of the Radio, the timer with time left and the 'live setName' appears. If user will press on this timeline it will expand and schedule of the Radio will appear. The live setName will be marked `green`.
If Radio is not live the background color of the bottom part will be `gray'` If user will press the "SHOW SCHEDULE" btn, he/she will see the schedule for this Radio.

# MISC:

## Folder structure:

I choose a folder structure based on features.
Each specific feature may contain it's own components/hooks/ etc.
The utils, services are defined for whole app.
src/features/common folder contains UI components that might be reused.

## Animations:

Take a look at `src -> features -> common -> schedule.tsx`
There is a place when 'react-native-reanimated' library is used.
I want to mention that all the animations/related calculations (such row 60 for instance)
Are computed by UI thread and not affecting JS-thread by using 'worklets'
See more: https://blog.swmansion.com/introducing-reanimated-2-752b913af8b3

## State machine for 'time passed' timer:

I want to specially mention the state machined for timer.
Since we're using react there is a very good option to solve those kinds of problem - by using custom hooks.
Please take a look: `src/Features/Radio/hooks/useEnhancedScheduleData.ts`
This hooks is computing such fileds as:

- time passed
- active setName
- is Radio itself is live

This hook can contain more Radio-specific data, but even on its last state it may be reused for each radio from list.

## QA:

Tested on Iphone 12 Pro Max (real device), Samsung S10 (real device)
