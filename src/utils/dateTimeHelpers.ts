import moment from 'moment';

export const calculateTimeDurationByTimestamp = (timestamp: string) => {
  const duration = moment.duration(
    moment.unix(parseInt(timestamp)).diff(moment()),
  );
  const hours = duration.hours();
  const minutes = duration.minutes();
  const seconds = duration.seconds();

  return moment([hours, minutes, seconds], 'h:m:s').format('HH:mm:ss');
};
