import {Dimensions} from 'react-native';

export const CARD_HEIGHT = Dimensions.get('screen').height * 0.3;
export const CARD_WIDTH = Dimensions.get('screen').width * 0.9;

export const ACTIVE_SET_BACKGROUND_COLOR = '#bb03a0';
export const UPCOMING_SET_BACKGROUND_COLOR = '#616161';
