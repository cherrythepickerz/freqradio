import moment, {Moment} from 'moment';
import {radioDataType} from '../features/Radio';
import {radio} from './dataProvier';

export type transformedDataForSchedule = {
  duration: number;
  id: string;
  name: string;
  timestamp: number;
}[];

export const transformDataForSchedule = (data: typeof radio['schedule']) => {
  //   const transformedData = tranformData(data);
  //   const isSetLive = transformedData.map(s => {
  //     const isLive = moment().isAfter(moment.unix(s.timestamp))
  //  return {
  //     isLive: !!isLive,
  //     activeSetName: s[]
  // }
  // });
  //   const timestaps = Object.keys(data);
  //   console.log(timestaps);
  //   const liveSet = timestaps.find(ts =>
  //     moment().isAfter(moment.unix(parseInt(ts))),
  //   );
  //   if (!liveSet) {
  //     return {isLive: false};
  //   }
  //   const activeSetName = data[liveSet].name;
  //   return {isLive: !!liveSet, activeSetName};
  //   return {
  //       isLive: isSetLive,
  //       activeSetName: '',
  //       schedule: [{
  //         duration: number;
  //         id: string;
  //         name: string;
  //         timestamp: string;
  //       }]
  //   }
};

const tranformData = (
  schedule: radioDataType['schedule'],
): transformedDataForSchedule => {
  return Object.keys(schedule).map(set => {
    return {
      ...schedule[set],
      timestamp: parseInt(set),
    };
  });
};
