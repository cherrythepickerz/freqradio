import moment from 'moment';
import {radioDataType} from '../features/Radio';

const set11 = moment().unix();
const set12 = moment().add(10, 'seconds').unix();
const set13 = moment().add(20, 'seconds').unix();

const set21 = moment().unix();
const set22 = moment().add(5, 'seconds').unix();
const set23 = moment().add(10, 'seconds').unix();

export const radio = [
  {
    title: 'Monday Happy Time',
    description: 'mingle with people with the same music taste as you',
    content: 'Tel Aviv’s Top notch electronic music live event hosted by',
    schedule: {
      [set11]: {
        id: 'set1',
        name: 'TLV summer set',
        duration: 10,
      },
      [set12]: {
        id: 'set2',
        name: 'Petach Tikwa Super Set',
        duration: 10,
      },
      [set13]: {
        id: 'set3',
        name: 'Eilat & Friends Set',
        duration: 21,
      },
    },
    coverImage:
      'https://res.cloudinary.com/dyswk0o6o/image/upload/v1612719493/event_main_image_2x_pbymok.png',
  },
  {
    title: 'Whiteboard time',
    description: 'Touch your feelings using whiteboard',
    schedule: {
      [set21]: {
        id: 'set1',
        name: 'DJ Marker set',
        duration: 5,
      },
      [set22]: {
        id: 'set2',
        name: 'Time complexity ultra set',
        duration: 5,
      },
      [set23]: {
        id: 'set3',
        name: 'Eilat & Friends Set',
        duration: 5,
      },
    },
    coverImage:
      'https://cdn.shopify.com/s/files/1/0537/4781/products/Whiteboard_grande.jpg?v=1572429283',
  },
];
