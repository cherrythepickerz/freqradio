import React from 'react';
import {StyleSheet, View} from 'react-native';

export default () => {
  return <View style={styles.divider} />;
};

const styles = StyleSheet.create({
  divider: {
    height: 1,
    width: '100%',
    backgroundColor: 'white',
    marginTop: 4,
    marginBottom: 8,
  },
});
