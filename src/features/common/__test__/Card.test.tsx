import React from 'react';
import Card from '../Card';
import {render} from '@testing-library/react-native';
import moment from 'moment';

const setKey1 = moment().unix();

const mockedProps = {
  title: 'Some title',
  description: 'Some description',
  content: 'Some content',
  coverImage: 'image_url',
  schedule: {
    [setKey1]: {
      id: 'some setId',
      name: 'some setName',
      duration: 1000,
    },
  },
};

describe('<Card />', () => {
  test('Matching snapshot', () => {
    const {toJSON} = render(<Card data={mockedProps} />);

    expect(toJSON()).toMatchSnapshot();
  });

  test('elements renders correctly', () => {
    const {getByTestId} = render(<Card data={mockedProps} />);

    const cardImageUri = getByTestId('card-background-image').props.source.uri;
    const cardTitleText = getByTestId('card-title').children[0];
    const cardDescriptiomText = getByTestId('card-description').children[0];
    const cardContentText = getByTestId('card-content').children[0];

    expect(cardImageUri).toEqual(mockedProps.coverImage);
    expect(cardTitleText).toEqual(mockedProps.title);
    expect(cardDescriptiomText).toEqual(mockedProps.description);
    expect(cardContentText).toEqual(mockedProps.content);
  });
});
