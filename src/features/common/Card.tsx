import React from 'react';
import {View, Image, StyleSheet, Text} from 'react-native';
import {CARD_HEIGHT, CARD_WIDTH} from '../../utils/const';
import {radioDataType} from '../Radio';
import useEnhncedScheduleData from '../Radio/hooks/useEnhancedScheduleData';
import Schedule from './Schedule';

interface Props {
  width?: string | number;
  height?: string | number;
  isLive?: boolean;
  data: radioDataType;
}

export default ({width = CARD_WIDTH, height = CARD_HEIGHT, data}: Props) => {
  const {title, description, content, coverImage, schedule} = data;
  const {timeDuration, activeSetName, isRadioLive} = useEnhncedScheduleData(
    schedule,
  );

  return (
    <>
      <View style={[styles.container, {height, width}]}>
        <Image
          style={[
            styles.img,
            {height, width, opacity: isRadioLive ? 0.6 : 0.3},
          ]}
          testID="card-background-image"
          resizeMode={'cover'}
          source={{uri: coverImage}}
        />
        <View style={styles.txtContainer}>
          <Text testID="card-title" style={styles.txtTitle}>
            {title}
          </Text>
          <Text testID="card-description" style={styles.txtCaption}>
            {description}
          </Text>
          <Text testID="card-content" style={styles.txtCaption}>
            {content}
          </Text>
        </View>
      </View>
      <Schedule
        timeDuration={timeDuration}
        activeSetName={activeSetName}
        schedule={schedule}
        isLive={isRadioLive}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 12,
    marginTop: 12,
  },
  img: {
    position: 'absolute',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    backgroundColor: 'black',
  },
  txtContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  txtTitle: {
    fontSize: 24,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'right',
    alignSelf: 'flex-end',
  },
  txtCaption: {
    fontSize: 12,
    color: 'white',
    alignSelf: 'flex-end',
    textAlign: 'right',
  },
});
