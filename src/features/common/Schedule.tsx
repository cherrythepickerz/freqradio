import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import Animated, {
  measure,
  useSharedValue,
  withSpring,
  withTiming,
  useAnimatedRef,
  useDerivedValue,
  runOnUI,
  useAnimatedStyle,
} from 'react-native-reanimated';
import {
  ACTIVE_SET_BACKGROUND_COLOR,
  UPCOMING_SET_BACKGROUND_COLOR,
} from '../../utils/const';
import {radioDataType} from '../Radio';

export default (props: {
  schedule: radioDataType['schedule'];
  isLive: boolean;
  activeSetName: string;
  timeDuration: string;
}) => {
  const {schedule, isLive, activeSetName, timeDuration} = props;
  const [isOpen, setOpen] = useState<boolean>(false);
  const [reanimatedComponentRendered, setAlreadyRender] = useState<boolean>(
    false,
  );

  const aref = useAnimatedRef<View>();

  const open = useSharedValue(false);
  const progress = useDerivedValue(() =>
    open.value
      ? withSpring(1, {
          damping: 14,
          stiffness: 150,
        })
      : withTiming(0),
  );
  const height = useSharedValue(0);
  const animatedStyles = useAnimatedStyle(() => ({
    height: height.value * progress.value + 1,
    opacity: progress.value === 0 ? 0 : 1,
  }));

  useEffect(() => {
    if (!reanimatedComponentRendered) return;
    runOnUI(() => {
      'worklet';
      height.value = measure(aref).height;
    })();
  }, [reanimatedComponentRendered]);

  const renderTimeline = () => {
    return (
      <TouchableWithoutFeedback
        style={{height: 30}}
        onPress={() => {
          open.value = !open.value;
          setOpen(open => !open);
        }}>
        <View style={styles.timelineContainer}>
          {isLive ? (
            <>
              <Text style={styles.txt}>LIVE: </Text>
              <Text
                style={styles.txt}>{`${activeSetName} - ${timeDuration}`}</Text>
            </>
          ) : (
            <Text style={styles.txt}>
              {!isOpen ? 'SHOW SCHEDULE' : 'CLOSE SCHEDULE'}
            </Text>
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const renderSchedule = () => {
    return (
      <Animated.View style={[animatedStyles, {overflow: 'hidden'}]}>
        <View onLayout={() => setAlreadyRender(true)} ref={aref}>
          {Object.keys(schedule).map((item: string, index: number) => {
            const setItem = schedule[item];
            const isSetLive = setItem.name === activeSetName;

            if (!setItem) {
              return;
            }
            return (
              <View style={styles.scheduleRow} key={index}>
                <Text
                  style={[
                    styles.scheduleRowTxt,
                    {fontWeight: isSetLive ? 'bold' : 'normal'},
                    {color: isSetLive ? '#00FF00' : '#ececec'},
                  ]}>
                  {setItem.name}
                </Text>
              </View>
            );
          })}
        </View>
      </Animated.View>
    );
  };

  return (
    <View
      style={[
        styles.gradientBar,
        {
          backgroundColor: isLive
            ? ACTIVE_SET_BACKGROUND_COLOR
            : UPCOMING_SET_BACKGROUND_COLOR,
        },
      ]}>
      {renderSchedule()}
      {renderTimeline()}
    </View>
  );
};

const styles = StyleSheet.create({
  gradientBar: {
    marginTop: 0,
    marginBottom: 12,
    borderBottomStartRadius: 12,
    borderBottomEndRadius: 12,
    backgroundColor: 'purple',
    paddingHorizontal: 12,
    paddingVertical: 12,
  },
  scheduleRow: {
    height: 45,
    alignItems: 'center',
  },
  timelineContainer: {
    marginTop: 8,
    flexDirection: 'row',
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  txt: {
    fontSize: 14,
    color: 'white',
  },
  scheduleRowTxt: {
    fontSize: 14,
    color: 'white',
  },
});
