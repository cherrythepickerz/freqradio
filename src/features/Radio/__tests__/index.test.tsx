import React from 'react';
import Radio from '../index';
import {render} from '@testing-library/react-native';

describe('<Radio />', () => {
  const {toJSON} = render(<Radio />);

  test('Matching snapshot', () => {
    expect(toJSON()).toMatchSnapshot();
  });
});
