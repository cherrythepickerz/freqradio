import {renderHook, act} from '@testing-library/react-hooks';
import {waitFor} from '@testing-library/react-native';
import moment from 'moment';
import useEnhancedScheduleData from '../useEnhancedScheduleData';

const setKey1 = moment().unix();
const setKey2 = moment().add(5, 'seconds').unix();

const hookMockProps = {
  [setKey1]: {
    id: 'id1',
    name: 'someName1',
    duration: 5,
  },
  [setKey2]: {
    id: 'id2',
    name: 'someName2',
    duration: 5,
  },
};

describe('', () => {
  test('Should return correct values after initialization', async () => {
    const {result, waitForNextUpdate} = renderHook(
      () => {},
      // useEnhancedScheduleData(hookMockProps),
    );
  });
});
