import moment from 'moment';
import React, {useEffect, useRef} from 'react';
import {radioDataType} from '..';
import {calculateTimeDurationByTimestamp} from '../../../utils/dateTimeHelpers';

export default (schedule: radioDataType['schedule']) => {
  const initialEnhancedDataState = {
    activeSetId: '',
    timeDuration: '0',
    activeSetName: '',
    isRadioLive: false,
  };

  const timer = useRef<any>();
  const [enhancedData, setEnhancedData] = React.useState<{
    activeSetId: string;
    timeDuration: string;
    activeSetName: string;
    isRadioLive: boolean;
  }>(initialEnhancedDataState);

  useEffect(() => {
    setActiveSet();
  }, []);

  useEffect(() => {
    if (enhancedData.isRadioLive) {
      timer.current = setInterval(() => {
        setActiveSet();
      }, 1000);
    }

    return () => clearInterval(timer.current);
  }, [
    enhancedData.activeSetName,
    enhancedData.timeDuration,
    enhancedData.isRadioLive,
  ]);

  const setActiveSet = () => {
    const keys = Object.keys(schedule);
    const timestaps = keys.map(ts => moment.unix(parseInt(ts)));
    const minTime = moment.min(timestaps);
    const lastSetStartTime = moment.max(timestaps);
    const maxTime = moment(lastSetStartTime).add(
      schedule[lastSetStartTime.unix()].duration,
      'seconds',
    );
    const isRadioLive = moment().isBetween(minTime, maxTime);

    if (!isRadioLive) {
      setEnhancedData(initialEnhancedDataState);

      return;
    }

    const liveSet = keys.find(ts =>
      moment().isBetween(
        moment.unix(parseInt(ts)),
        moment.unix(parseInt(ts)).add(schedule[ts].duration, 'seconds'),
      ),
    );

    if (!liveSet) {
      setEnhancedData(initialEnhancedDataState);

      return;
    }

    setEnhancedData({
      isRadioLive: true,
      activeSetId: schedule[liveSet].id,
      timeDuration: calculateTimeDurationByTimestamp(liveSet),
      activeSetName: schedule[liveSet].name,
    });
  };

  return enhancedData;
};
