import React from 'react';
import {View, Text, StyleSheet, Image, FlatList} from 'react-native';
import {radio} from '../../services/dataProvier';
import Card from '../common/Card';

const imgUrl = require('../../assets/photo1.jpeg');

export type radioDataType = {
  title: string;
  description?: string;
  content?: string;
  coverImage: string;
  schedule: {
    [key: string]: {
      id: string;
      name: string;
      duration: number;
    };
  };
};

export default () => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.img}
        resizeMode={'cover'}
        source={imgUrl}
        blurRadius={20}
      />
      <View style={styles.cardContainer}>
        <Text style={styles.title}>RADIO</Text>
        <FlatList
          style={styles.flatlist}
          data={radio}
          keyExtractor={item => item.title}
          renderItem={({index, item}) => {
            return <Card key={index} data={item} />;
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
  },
  flatlist: {
    flex: 1,
    paddingBottom: 40,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  img: {
    position: 'absolute',
    height: '120%',
    width: '100%',
    backgroundColor: 'black',
    opacity: 0.3,
  },
  cardContainer: {
    marginTop: 60,
  },
});
